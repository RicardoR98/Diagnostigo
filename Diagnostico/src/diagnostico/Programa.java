/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package diagnostico;

import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

/**
 *
 * @author rodri
 */
public class Programa {
    // Declaracion de variables que usaremos como contadores
    private int inve, pon, talle, gGene;
    // Instanciacion de la clase scanner para resivir datos del teclado
    Scanner s = new Scanner(System.in);
    
    // Declaracion de ArrayList dependiendo su clase para poderlos guardar e imprimir
    ArrayList<Investigadores> investigadores = new ArrayList<>();
    ArrayList<Ponentes> ponentes = new ArrayList<>();
    ArrayList<Talleristas> talleristas = new ArrayList<>();
    ArrayList<PGeneral> pGeneral = new ArrayList<>();
    
    public Programa() {// Constructor que se manda llamar al momento de instanciar la clase
        //Llamada al metodo menu oara visualizar en consola las instrucciones
        menu();
    }
    
    private void menu() {// Metodo que imprimira que seleccion se puede realizar
        System.out.println("********************************************");
        System.out.println("Bienvenido al sistema contador de asistentes");
        System.out.println("********************************************");
        System.out.println("Porfavor seleccione que tipo de asistencia registrara");
        System.out.println("1.- Investigador");
        System.out.println("2.- Ponente");
        System.out.println("3.- Tallerista");
        System.out.println("4.- Gente en general");
        System.out.print("Seleccion: ");int seleccion = s.nextInt();// Instruccion donde el sistema esperara por un dato del teclado
        // Switch encargado de manejar la seleccion especificada
        switch(seleccion){
            case 1:
                investigador();
                break;
            case 2:
                ponente();
                break;
            case 3:
                tallerista();
                break;
            case 4:
                gGeneral();
                break;
            default:
                // Se lanzara si es que seleccionan 0 o un numero mayor a 4
                limpiar();
                System.out.println("Error: Opcion incorrecta....");
                menu();
                break;
        }
    }
    
    private void investigador(){
        System.out.print("Nombres: ");String nombre = s.nextLine();
        System.out.print("Apellido Paterno: ");String aPaterno = s.nextLine();
        System.out.print("Apellido Materno: ");String aMaterno = s.nextLine();
        System.out.print("Fecha de su Invento: ");String fInvento = s.nextLine();
        System.out.print("Nombre de su Invento: ");String invento = s.nextLine();
        // Linea donde se guarda el objeto dentro del ArrayList para hacer una lista e imprimirla
        investigadores.add(new Investigadores(nombre, aMaterno, aPaterno, fInvento, invento));
        inve++;
        limpiar();
        preguntar();
    }
    
    private void ponente(){
        System.out.print("Nombres: ");String nombre = s.nextLine();
        System.out.print("Apellido Paterno: ");String aPaterno = s.nextLine();
        System.out.print("Apellido Materno: ");String aMaterno = s.nextLine();
        System.out.print("Tema a Hablar: ");String temaHabla = s.nextLine();
        System.out.print("Hora de su Presentacion: ");String hora = s.nextLine();
        // Linea donde se guarda el objeto dentro del ArrayList para hacer una lista e imprimirla
        ponentes.add(new Ponentes(nombre, aMaterno, aPaterno, temaHabla, hora));
        pon++;
        limpiar();
        preguntar();
    }
        
    private void tallerista(){
        System.out.print("Nombres: ");String nombre = s.nextLine();
        System.out.print("Apellido Paterno: ");String aPaterno = s.nextLine();
        System.out.print("Apellido Materno: ");String aMaterno = s.nextLine();
        System.out.print("Nombre del Taller: ");String nomTaller = s.nextLine();
        System.out.print("Numero del Salon Donde Sera el Taller: ");String numeroTaller = s.nextLine();
        // Linea donde se guarda el objeto dentro del ArrayList para hacer una lista e imprimirla
        talleristas.add(new Talleristas(nombre, aMaterno, aPaterno, nomTaller, numeroTaller));
        talle++;
        limpiar();
        preguntar();
    }
            
    private void gGeneral(){
        System.out.print("Nombres: ");String nombre = s.nextLine();
        System.out.print("Apellido Paterno: ");String aPaterno = s.nextLine();
        System.out.print("Apellido Materno: ");String aMaterno = s.nextLine();
        System.out.print("Numero de Voleto: ");String nVoleto = s.nextLine();
        System.out.print("Numero de Ssiento: ");String nAsiento = s.nextLine();
        // Linea donde se guarda el objeto dentro del ArrayList para hacer una lista e imprimirla
        pGeneral.add(new PGeneral(nombre, aMaterno, aPaterno, nVoleto, nAsiento));
        gGene++;
        limpiar();
        preguntar();
    }
    
    private void preguntar() {
        
        int res = 0;
        
        try {
            System.out.println("¿Deceas registrar otra asistencia?");
            System.out.println("");
            System.out.println("1.- SI");
            System.out.println("2.- NO");
            System.out.print("Seleccion: ");res = s.nextInt();
            limpiar();
        } catch (Exception e) {
            limpiar();
            System.out.println("Solo selecciona un numero, no caracteres especiales");
            preguntar();
        }

        // Switch encargado de manejar la seleccion entre si quieren registrar otra persona o no
        switch (res) {
            case 1:
                menu();
                break;
            case 2:
                imprimir();
            default:
                // Se lanzara si es que seleccionan 0 o un numero mayor a 2
                limpiar();
                System.out.println("Solo selecciona un numero entre 1 y 2");
                preguntar();
                break;
        }
    }
    
    public void limpiar()// Metodo para crear saltos de lineas y aparentar que la terminal se limpio
    {
        for (int i=0; i <= 10; i++)
        {
            System.out.println("");
        }
    }

    private void imprimir() { // Metodo que se encargara de imprimir los resultados dados por el sistema
        limpiar();
        System.out.println("Lista de Asistencia:");
        // Bucles ForEach para imprimir los datos que guarda cada uno de los ArrayList
        investigadores.forEach((datos) -> {
            System.out.println(datos);
        });
        ponentes.forEach((datos) -> {
            System.out.println(datos);
        });
        talleristas.forEach((datos) -> {
            System.out.println(datos);
        });
        pGeneral.forEach((datos) -> {
            System.out.println(datos);
        });
        System.out.println("Asisitieron: ");
        System.out.println(inve+" investigadores");
        System.out.println(pon+" ponentes");
        System.out.println(talle+" talleristas");
        System.out.println(gGene+" gente en general");
        System.out.println("Gracias por usar nuestro sistema");
        System.out.println("Le atendio: ");
        Date fecha = DatosAdministrador.getInstance();
        System.out.println("Fecha de corte: " + fecha);
        System.exit(0); // Instruccion para cerrar una app JAVA
    }
   
}
