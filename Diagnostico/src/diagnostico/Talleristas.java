/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package diagnostico;

/**
 *
 * @author rodri
 */
public class Talleristas extends Persona{ //clase heredando de la clase padre Persona
    //Declaracion de variables
    private String nomTaller;
    private String numeroTaller;

    //Declaracion de constructor para llenado de datos
    public Talleristas(String nombre, String aMaterno, String aPaterno, String nomTaller, String numeroTaller) {
        super(nombre, aMaterno, aPaterno);
        this.nomTaller = nomTaller;
        this.numeroTaller = numeroTaller;
    }

    //Get y Set de cada uno de los atributos de la clase
    public String getNomTaller() {
        return nomTaller;
    }

    public void setNomTaller(String nomTaller) {
        this.nomTaller = nomTaller;
    }

    public String getNumeroTaller() {
        return numeroTaller;
    }

    public void setNumeroTaller(String numeroTaller) {
        this.numeroTaller = numeroTaller;
    }

    @Override
    public String toString() {
        return "Nombre Taller= " + nomTaller + ", Numero Taller= " + numeroTaller;
    }
    
}
